from tests.rpc import main
from zealous.rpc import RPCClientAppMixin
from zealous.rpc import RPCClientConf
from zealous.rpc import RPCServerAppMixin
from zealous.rpc import RPCServerConf
from zealous.rpc.asyncioq import AsyncioQRPCDriver


rpc_driver = AsyncioQRPCDriver(maxsize=1)


class App(RPCClientAppMixin[main.Boundary], RPCServerAppMixin[main.Boundary]):
    __rpc_client__ = RPCClientConf(driver=rpc_driver)
    __rpc_server__ = RPCServerConf(driver=rpc_driver, impl=main.BoundaryImpl())
