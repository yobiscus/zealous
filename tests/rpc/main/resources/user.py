import uuid

from datetime import datetime
from typing import Optional
from typing import Tuple

import zealous


class LookupSchema(zealous.Schema):
    user_id: uuid.UUID


class RegistrationSchema(zealous.Schema):
    username: str
    password: str
    full_name: Optional[str] = None


class Schema(zealous.Schema):
    user_id: uuid.UUID
    username: str
    registered_on: datetime


class User(zealous.Resource):
    # simple CRUD interfaces
    register: zealous.Interface[RegistrationSchema, Schema]
    get_details: zealous.Interface[LookupSchema, Schema]
    delete: zealous.Interface[LookupSchema, None]
    list: zealous.InterfaceNoBody[Tuple[Schema, ...]]

    # other interfaces to test behaviour of framework
    not_implemented: zealous.InterfaceNoBody[None]
