import asyncio

from typing import Iterator

import pytest


@pytest.fixture
def _event_loop(
    event_loop: asyncio.AbstractEventLoop,
) -> Iterator[asyncio.AbstractEventLoop]:
    yield event_loop
