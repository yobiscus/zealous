# Zealous Framework

[![pipeline status](https://gitlab.com/python-zealous/zealous/badges/master/pipeline.svg)](https://gitlab.com/python-zealous/zealous/badges/master/pipeline.svg)
[![coverage report](https://gitlab.com/python-zealous/zealous/badges/master/coverage.svg)](https://gitlab.com/python-zealous/zealous/badges/master/coverage.svg)

---

**Documentation:** <https://python-zealous.gitlab.io/zealous/>

**Source Code:** <https://gitlab.com/python-zealous/zealous>

---

Zealous is a framework that aims to simplify specification-driven development.

It is built from the ground up with emphasis on static type checking to ensure that the
specification is respected while developing around it.

The framework's core elements run in an asyncio event loop so concurrency is supported
out of the box.
