# AppMixins

AppMixins are the glue that connect the App to the Boundary. They can provide
client-side functionality to enable communication with the boundary, or they can provide
server-side infrastructure to enable implementation of the boundary's business logic.

## Available AppMixins

* [RPCClientAppMixin](rpc-client.md)
* [RPCServerAppMixin](rpc-server.md)

## Planned AppMixins

* CLIClientAppMixin
* CLIServerAppMixin
* DBClientAppMixin
* HTTPClientAppMixin
* HTTPServerAppMixin
